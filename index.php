<?php

$pathinfo = isset($_SERVER['PATH_INFO']) ? $_SERVER['PATH_INFO'] : $_SERVER['REDIRECT_URL'];
$params = preg_split('|/|', $pathinfo, -1, PREG_SPLIT_NO_EMPTY);
$username = $params[0];

require_once 'database.php';

if (empty($username)) {
    echo '<h3>' . 'User list' . '<h3>';
    $db = new Database();
    $users = $db->getUsers();
    echo '<table id="users">';
    foreach ($users as $user) {
        echo '<tr>';
        echo '<td>' . $user['id'] . ') ' . '</td>';
        echo '<td>' . $user['username'] . '</td>';
        echo '<td>' . ' <a href="' . $_SERVER['PHP_SELF'] . $user['username'] . '">details</a>' . "<br>" . '</td>';
        echo '</tr>';
    }
    echo '</table>';

    echo "<br>" . 'SEARCH';
    echo '<form action="../index.php/">';
    echo '<input type="text" name="keyword">';
    echo '<select name="username">';
    foreach ($users as $user) {
        echo '<option value="' . $user['id'] . '">' . $user['username'] . '</option>';
    }
    echo '</select>';
    echo '<input type="hidden" name="pagination" value="2">';
    echo '<input type="submit" value="Submit">';
    echo '</form>';

    if(isset($_GET['keyword']))
    {
        $tweetsMatched = $db->getTweetsMatched($_GET['username'], $_GET['keyword']);
        for($i=$_GET['pagination']-2; $i<$_GET['pagination']; $i++){
            echo $tweetsMatched[$i]['text'] . "<br>";
        }
        if($_GET['pagination']-2 > 0){
            echo '<a href="'. $_SERVER['PHP_SELF'] . '?keyword=&username=1&pagination=' . ($_GET['pagination']-2) . '">previous</a> ';
        }
        if (array_key_exists($_GET['pagination'], $tweetsMatched)) {
            echo ' <a href="'. $_SERVER['PHP_SELF'] . '?keyword=&username=1&pagination=' . ($_GET['pagination']+2) .'">next</a><br>';
        }
    }
} else {
    $db = new Database();
    $user = $db->getUser($username);
    $tweets = $db->getUserTweets($user['id']);

    if (!empty($user)) {
        echo '<h3>' . 'User details' . '<h3>';
        echo 'username: ' . $user['username'] . "<br>" .
            'id: ' . $user['id'] . "<br><br>";

        foreach ($tweets as $tweet) {
            echo $tweet['text'] . "<br>";
        }
    } else {
        echo '<h3>' . 'User added' . '<h3>';
        $db->saveUser($username);
    }

    echo '<br><a href="../index.php/">user list</a><br>';
}
?>