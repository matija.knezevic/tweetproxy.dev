<?php

class Database
{

    public $_host = "localhost";
    public $_user = "mknezevi1";
    public $_database = "natjecaji";
    public $_pass = "mmmmmmmm";

    function dbConnect()
    {
        $conn = new mysqli($this->_host, $this->_user, $this->_pass, $this->_database);
        if ($conn->connect_error) {
            echo $conn->connect_error;
        } else {
            $conn->set_charset("utf8");
        }
        return $conn;
    }

    function getUsers()
    {
        $users = array();
        $conn = new mysqli($this->_host, $this->_user, $this->_pass, $this->_database);
        if ($conn->connect_error) {
            echo $conn->connect_error;
        } else {
            $conn->set_charset("utf8");
            $sql = "SELECT * FROM users";
            $rs = $conn->query($sql);
            if ($rs) {
                while ($row = $rs->fetch_assoc()) {
                    $users[] = $row;
                }
            } else {
                echo $conn->error . " sql:" . $sql;
            }
        }
        $conn->close();
        return $users;
    }

    function getUser($username)
    {
        $conn = new mysqli($this->_host, $this->_user, $this->_pass, $this->_database);
        if ($conn->connect_error) {
            echo $conn->connect_error;
        } else {
            $conn->set_charset("utf8");
            $sql = "SELECT * FROM users WHERE username = '$username'";
            $rs = $conn->query($sql);
            if ($rs) {
                $user = $rs->fetch_assoc();
            } else {
                echo $conn->error . " sql:" . $sql;
            }
        }
        $conn->close();
        return $user;
    }

    function getUserTweets($user_id) {
        $configNumber = 20;
        $userTweets = array();
        $conn = new mysqli($this->_host, $this->_user, $this->_pass, $this->_database);
        if ($conn->connect_error) {
            echo $conn->connect_error;
        } else {
            $conn->set_charset("utf8");
            $sql = "SELECT * FROM tweets WHERE users_id = '$user_id' ORDER BY id DESC LIMIT $configNumber";
            $rs = $conn->query($sql);
            if ($rs) {
                while ($row = $rs->fetch_assoc()) {
                    $userTweets[] = $row;
                }
            } else {
                echo $conn->error . " sql:" . $sql;
            }
        }
        $conn->close();
        return $userTweets;
    }

    function getTweetsMatched($user_id, $keyword) {
        $configNumber = 20;
        $userTweets = array();
        $conn = new mysqli($this->_host, $this->_user, $this->_pass, $this->_database);
        if ($conn->connect_error) {
            echo $conn->connect_error;
        } else {
            $conn->set_charset("utf8");
            $sql = "SELECT * FROM tweets WHERE users_id = '$user_id' OR text LIKE '%$keyword%'";
            $rs = $conn->query($sql);
            if ($rs) {
                while ($row = $rs->fetch_assoc()) {
                    $userTweets[] = $row;
                }
            } else {
                echo $conn->error . " sql:" . $sql;
            }
        }
        $conn->close();
        return $userTweets;
    }

    function saveUser($username)
    {
        $conn = new mysqli($this->_host, $this->_user, $this->_pass, $this->_database);
        if ($conn->connect_error) {
            echo $conn->connect_error;
        } else {
            $conn->set_charset("utf8");
            $sql = 'INSERT INTO users(username) ';
            $sql .= 'VALUES ("' . $username . '")';
            $rs = $conn->query($sql);
            if ($rs) {
                //echo "ok";
            } else {
                echo $conn->error . " sql:" . $sql;
            }
        }
        $conn->close();
        return;
    }

}

?>