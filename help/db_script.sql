
-- -----------------------------------------------------

-- Table `users`

-- -----------------------------------------------------

DROP TABLE IF EXISTS `users` ;



CREATE  TABLE IF NOT EXISTS `users` (

  `id` INT NOT NULL AUTO_INCREMENT ,

  `username` VARCHAR(45) NOT NULL ,

  PRIMARY KEY (`id`) )

ENGINE = InnoDB;





-- -----------------------------------------------------

-- Table `tweets`

-- -----------------------------------------------------

DROP TABLE IF EXISTS `tweets` ;



CREATE  TABLE IF NOT EXISTS `tweets` (

  `id` INT NOT NULL AUTO_INCREMENT ,

  `text` VARCHAR(45) NOT NULL ,

  `users_id` INT NOT NULL ,

  PRIMARY KEY (`id`) ,

  INDEX `fk_tweets_users_idx` (`users_id` ASC) ,

  CONSTRAINT `fk_tweets_users`

    FOREIGN KEY (`users_id` )

    REFERENCES `users` (`id` )

    ON DELETE NO ACTION

    ON UPDATE NO ACTION)

ENGINE = InnoDB;